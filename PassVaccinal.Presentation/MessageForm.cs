﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PassVaccinal.Presentation
{
    public partial class MessageForm : Form
    {

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private delegate void CloseDelegate();
        public MessageForm(string message)
        {
            InitializeComponent();
            this.label1.Text = message;

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        private static MessageForm messageForm;

        static public void ShowMessage(string message)
        {
            // Make sure it is only launched once.    
            if (messageForm != null) return;
            messageForm = new MessageForm(message);
            Thread thread = new Thread(new ThreadStart(MessageForm.ShowMessage));
            thread.IsBackground = true;
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            /* Task.Delay(5000);
            CloseForm();*/
        }

        static private void ShowMessage()
        {
            if (messageForm != null) Application.Run(messageForm);
        }

        static public void CloseForm()
        {
            messageForm?.Invoke(new CloseDelegate(MessageForm.CloseFormInternal));
        }

        static private void CloseFormInternal()
        {
            if (messageForm != null)
            {
                messageForm.Close();
                messageForm = null;
            };
        }
    }
}
