﻿namespace PassVaccinal.Presentation
{
    using PassVaccinal.Presentation.Components;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System;
    using System.Drawing;
    using System.Windows;
    using System.Windows.Forms;
    using System.Collections;
    using PassVaccinal.Presentation.Service;
    using PassVaccinal.Presentation.Models.Entity;

    partial class PassVaxForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WindowState = FormWindowState.Maximized;
            this.Size = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            this.tableLayoutPanel1 = new TableLayoutPanel();
            this.mainSplitContainer = new SplitContainer();
            this.bodySplitContainer = new SplitContainer();
            this.headerContainer = new HeaderComponent();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bodySplitContainer)).BeginInit();
            this.bodySplitContainer.Panel1.SuspendLayout();
            this.bodySplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            this.tableLayoutPanel1.Location = new Point(357, 21);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new Size(395, 596);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.Location = new Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            this.mainSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.headerContainer);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.bodySplitContainer);
            this.mainSplitContainer.Size = this.Size;// new Size(1332, 725);
            this.mainSplitContainer.SplitterDistance = 70;
            this.mainSplitContainer.TabIndex = 0;

            
            // 
            // bodySplitContainer
            // 
            this.bodySplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bodySplitContainer.Location = new Point(0, 0);
            this.bodySplitContainer.Name = "bodySplitContainer";
            this.bodySplitContainer.Size = new Size(1332, 569);
            this.bodySplitContainer.SplitterDistance = (int)(this.bodySplitContainer.Size.Width * 0.2);
            this.bodySplitContainer.TabIndex = 0;
            //this.bodySplitContainer.Panel1.BackColor = Color.Green;

            // 
            // sideMenu1
            // 
            this.sideMenu = new SideMenu();
            this.sideMenu.Location = new Point(12, 15);
            this.sideMenu.Name = "sideMenu1";
            this.sideMenu.Size = this.bodySplitContainer.Panel1.Size;// new Size(350, 240);
            this.sideMenu.Width = this.bodySplitContainer.SplitterDistance;
            this.sideMenu.TabIndex = 0;
            this.sideMenu.OnMenuSelection += new EventHandler(this.sideMenu1_OnMenuSelection);
            this.sideMenu.Refresh();

            // 
            // bodySplitContainer.Panel1
            // 
            this.bodySplitContainer.Panel1.Controls.Add(this.sideMenu);
            //this.bodySplitContainer.AutoScaleMode
            
            //this.bodySplitContainer.Panel2.BackColor = Color.Red;
            // 
            // headerContainer
            // 
            //this.headerContainer.BackColor = Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.headerContainer.Location = new Point(2, 0);
            this.headerContainer.Name = "headerContainer";
            this.headerContainer.Size = this.Size;
            this.headerContainer.TabIndex = 0;
            this.headerContainer.OnRowChecked += new EventHandler(this.headerContainer_ReInit);
            Components.Add("header", this.headerContainer);
            
            
            //this.sideMenu.BackColor = Color.Blue;

            this.headerContainer.OnMenuSelection += new EventHandler(this.sideMenu1_OnMenuSelection);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //this.ClientSize = new Size(1332, 725);
            this.Controls.Add(this.mainSplitContainer);
            this.Name = "Form1";
            this.Text = "PassVax";
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.bodySplitContainer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bodySplitContainer)).EndInit();
            this.bodySplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private HeaderComponent headerContainer;

        private TableLayoutPanel tableLayoutPanel1;
        private SplitContainer mainSplitContainer;
        private SplitContainer bodySplitContainer;
        private SideMenu sideMenu;

        public static Dictionary<string, Control> Components { get; private set; } = new Dictionary<string, Control>();

        public static string ActivatedSideMenu { get; set; }
        public static string ActivatedHeaderMenu { get; set; }

        public static ArrayList SelectedObject { get;  set; }=new ArrayList();

        public static User CurrentUser { get; set; }
    }
}

