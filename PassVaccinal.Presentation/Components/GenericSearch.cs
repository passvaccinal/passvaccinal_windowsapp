﻿namespace PassVaccinal.Presentation.Components
{
    using PassVaccinal.Presentation.Models.Attributes;
    using PassVaccinal.Presentation.Service;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Forms;
    using System.Drawing;

    public partial class GenericSearch<T, U> : UserControl where U : IService<T>
    {
        private U service { get; set; }
        public GenericSearch(GenericList<T, U> list, U u, Size s)
        {
            this.service = u;
            this.genericList = list;
            if (s != null)
            {
                this.Size = s;
            }
            //this.Width = 300;
            this.genericList.Visible = false;
            InitializeComponent();
        }

        private void createForm()
        {
            var type = typeof(T);
            var props2 = type.GetProperties().Where(x => x.GetCustomAttributes(typeof(Searchable), true).Length > 0).ToList();
            var props = type.GetProperties().Where(x => x.GetCustomAttributes(typeof(Identifier), true).Length > 0).ToList();
            props.AddRange(props2);

            var i = 0;
            foreach (var prop in props)
            {
                var attr = prop.GetCustomAttributes(typeof(DisplayLabel), true);

                var hide = prop.GetCustomAttributes(typeof(Hide), true);
                if (hide.Length > 0)
                {
                    continue;
                }

                var label = new Label
                {
                    AutoSize = true,
                    Name = prop.Name,
                    Text = (attr.Length > 0) ? (attr[0] as DisplayLabel).Label : prop.Name,
                    Cursor = Cursors.Hand
                };

                var control = this.Display(prop);

                this.flowLayoutPanel.Controls.Add(label);
                this.flowLayoutPanel.Controls.Add(control);
                i++;
            }
            var searchButton = new Button
            {
                Text = "Lancer la recherche"
            };
            searchButton.Click += new EventHandler(this.searchClick);

            this.flowLayoutPanel.Controls.Add(searchButton);
        }

        private void searchClick(object sender, EventArgs e)
        {

            Splash.ShowSplashScreen();
            var dic = new Dictionary<PropertyInfo, object>();
            foreach (Control ctrl in this.flowLayoutPanel.Controls)
            {
                if (ctrl.Name.IndexOf("search_input_") > -1)
                {
                    var attributeName = ctrl.Name.Replace("search_input_", string.Empty);
                    var prop = typeof(T).GetProperty(attributeName);
                    var val = this.GetValue(ctrl, prop);
                    if (val != null)
                    {
                        dic.Add(prop, val);
                    }
                }
            }
            this.genericList.Visible = true;
            this.genericList.ReInit(this.Find(dic));
            Splash.CloseForm();
        }
        public List<T> Find(Dictionary<PropertyInfo, object> q)
        {
            var res = service.GetAll();
            if (q.Count == 0) return res;
            List<T> result = new List<T>();
            foreach (T obj in res)
            {
                foreach (var key in q)
                {
                    var objVal = key.Key.GetValue(obj);
                    if (objVal !=null && objVal.ToString().Contains(key.Value.ToString()))
                    {
                        result.Add(obj);
                    }
                }
            }
            return result;
        }
        private Control Display(PropertyInfo prop)
        {
            var typeName = prop.PropertyType.FullName;
            Control control;

            if (prop.PropertyType.IsEnum)
            {
                ComboBox control1 = new ComboBox
                {
                    DataSource = Enum.GetValues(prop.PropertyType)
                };

                return control1;
            }
            _ = prop.PropertyType.IsValueType;


            switch (typeName)
            {
                case "System.DateTime":
                case "System.Date":
                    control = new DateTimePicker
                    {
                        Name = $"search_input_{prop.Name}"
                    };

                    return control;
                case "System.Int32":
                    control = new NumericUpDown
                    {
                        Name = $"search_input_{prop.Name}"
                    };

                    return control;
                default:

                    control = new TextBox
                    {
                        Name = $"search_input_{prop.Name}",
                        Dock = DockStyle.Bottom
                    };

                    return control;
            }
        }

        private object GetValue(Control control, PropertyInfo prop)
        {
            var typeName = prop.PropertyType.FullName;
            if (prop.PropertyType.IsEnum)
            {
                return ((ComboBox)control).SelectedItem;
            }
            switch (typeName)
            {
                case "System.DateTime":
                case "System.Date":
                    var dateControl = control as DateTimePicker;

                    return dateControl.Value;
                case "System.Int32":

                    return int.Parse(control.Text);
                case "System.Guid":
                    if (Guid.TryParse(control.Text, out Guid guid)) {
                        return guid;
                    };
                    return null;// Guid.Parse();
                default:

                    return control.Text;
            }
        }
    }
}
