﻿namespace PassVaccinal.Presentation.Components
{
    using PassVaccinal.Presentation.Service;
    using System.Windows.Forms;
    partial class GenericForm<T,U> where U : IService<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new TableLayoutPanel();
            this.SuspendLayout();

            this.createForm();

            this.Controls.Add(this.tableLayoutPanel);
            this.AutoSize = true;
            this.Name = "GenericForm";
            this.ResumeLayout(false);
        }

        #endregion
        private TableLayoutPanel tableLayoutPanel;

    }
}
