﻿namespace PassVaccinal.Presentation.Components
{
    using PassVaccinal.Presentation.Helper;
    using PassVaccinal.Presentation.Models.Attributes;
    using PassVaccinal.Presentation.Service;
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class GenericForm<T, U> : UserControl where U : IService<T>
    {
        private T Item { get; set; }
        private U service { get; set; }
        public bool IsValid { get; private set; } = true;
        public bool IsCreation { get;  set; } = true;
        public Control container { get; set; }

        public GenericForm(T item, Control container, U u)
        {
            this.Item = item;
            this.service = u;
            this.container = container;
            InitializeComponent();
        }

        private void createForm()
        {
            var type = typeof(T);
            var props = type.GetProperties();
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = props.Length + 1;
            this.tableLayoutPanel.TabIndex = 0;
            this.tableLayoutPanel.Size = this.container.Size;
            var i = 0;
            foreach (var prop in props)
            {
                var attr = prop.GetCustomAttributes(typeof(DisplayLabel), true);
                var identifier = prop.GetCustomAttributes(typeof(Identifier), true);
                var hide = prop.GetCustomAttributes(typeof(Hide), true);

                var control = this.Display(prop);
                if (identifier.Length > 0)
                {
                    control.Enabled = false;
                }

                var label = new Label
                {
                    AutoSize = true,
                    Name = prop.Name,
                    Text = (attr.Length > 0) ? (attr[0] as DisplayLabel).Label : prop.Name,
                    Cursor = Cursors.Hand
                };

                if (hide.Length > 0)
                {
                    var hideValue = (hide[0] as Hide).show;
                    control.Visible = !hideValue;
                    label.Visible = !hideValue;
                }
                label.Click += new EventHandler(this.setFocus);
                this.tableLayoutPanel.Controls.Add(label, 0, i);
                this.tableLayoutPanel.Controls.Add(control, 1, i);
                i++;
            }
            var saveButton = new Button
            {
                Text = "Sauvegarder"
            };
            saveButton.Click += new EventHandler(this.saveClick);
            this.tableLayoutPanel.Controls.Add(saveButton, 1, i);
        }


        private void setFocus(object sender, EventArgs e)
        {
            Label lab = (Label)sender;
            foreach (Control ctrl in this.tableLayoutPanel.Controls)
            {
                if (ctrl.Name == $"input_{lab.Name}")
                {
                    ctrl.Focus();
                    return;
                }
            }
        }

        private void saveClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                foreach (Control ctrl in this.tableLayoutPanel.Controls)
                {
                    if (ctrl.Name.IndexOf("input_") > -1)
                    {
                        var attributeName = ctrl.Name.Replace("input_", string.Empty);
                        var prop = typeof(T).GetProperty(attributeName);
                        var value = this.GetValue(ctrl, prop);
                        if (value != null)
                        {
                            prop.SetValue(this.Item, this.GetValue(ctrl, prop));

                        }
                        else
                        {
                            //prop.SetValue(this.Item,def)
                        }
                    }
                }
                var message = string.Empty;
                Splash.ShowSplashScreen();
                if (IsCreation)
                {
                    message = "creation avec succes";
                    this.service.Create(this.Item);
                }
                else
                {
                    message = "mis a jour fait";
                    this.service.Update(this.Item);

                }
                Splash.CloseForm();
                MessageForm.ShowMessage(message);
                Task.Delay(5000);
                MessageForm.CloseForm();
                Util.ResetAllControls<T>(this.tableLayoutPanel);
            }
        }

        private Control Display(PropertyInfo prop)
        {
            var typeName = prop.PropertyType.FullName;
            Control control;

            if (prop.PropertyType.IsEnum)
            {
               
                ComboBox control1 = new ComboBox
                {
                    Name = $"input_{prop.Name}",
                    DataSource = Enum.GetValues(prop.PropertyType)
                };

                return control1;
            }
            _ = prop.PropertyType.IsValueType;
            var isPwd = prop.GetCustomAttributes(typeof(Password), true);
            if (isPwd.Length > 0)
            {
                return new TextBox
                {
                    Name = $"input_{prop.Name}",
                    PasswordChar = '*'
                };
            }
            var isAutoComplete = prop.GetCustomAttributes(typeof(AutocompleteDisplay), true);
            if (isAutoComplete.Length > 0)
            {
                var source = PatientService.Service.GetAll().Select(x => $"{x.FirstName}-{x.LastName}").ToArray();
                AutoCompleteStringCollection source2 = new AutoCompleteStringCollection();
                source2.AddRange(source);
                return new TextBox
                {
                    Name = $"input_{prop.Name}",
                    AutoCompleteMode = AutoCompleteMode.Suggest,
                    AutoCompleteSource=AutoCompleteSource.CustomSource,
                    AutoCompleteCustomSource=source2
                   
                };
            }

            switch (typeName)
            {
                case "System.DateTime":
                case "System.Date":
                    control = new DateTimePicker
                    {
                        Name = $"input_{prop.Name}"
                    };
                    if (prop.GetValue(this.Item) != null)
                    {
                        control.Text = prop.GetValue(this.Item).ToString();
                    }
                    return control;
                case "System.Int32":
                    control = new NumericUpDown
                    {
                        Name = $"input_{prop.Name}"
                    };

                    if (prop.GetValue(this.Item) != null)
                    {
                        control.Text = prop.GetValue(this.Item).ToString();
                    }
                    return control;
                default:
                    control = new TextBox
                    {
                        Name = $"input_{prop.Name}"
                    };

                    if (prop.GetValue(this.Item) != null)
                    {
                        control.Text = prop.GetValue(this.Item).ToString();
                    }
                    return control;
            }
        }

        private object GetValue(Control control, PropertyInfo prop)
        {
            var typeName = prop.PropertyType.FullName;
            if (prop.PropertyType.IsEnum)
            {               
                return ((ComboBox)control).SelectedItem;
            }
                switch (typeName)
            {
                case "System.DateTime":
                case "System.Date":
                    var dateControl = control as DateTimePicker;

                    return dateControl.Value;
                case "System.Int32":

                    return int.Parse(control.Text);
                case "System.Guid":
                    var isAutoComplete = prop.GetCustomAttributes(typeof(AutocompleteDisplay), true);
                    if (isAutoComplete.Length > 0)
                    {
                        var firstName = control.Text.Split('-')[0];
                        var lastName = control.Text.Split('-')[1];
                        var source = PatientService.Service.GetAll().FirstOrDefault(x =>x.FirstName==firstName&&x.LastName==lastName);
                        return source.Id;
                    }
                    return Guid.Parse(control.Text);
                default:

                    return string.IsNullOrEmpty(control.Text) ? null:control.Text ;
            }
        }
    }
}
