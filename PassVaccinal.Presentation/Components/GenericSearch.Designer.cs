﻿namespace PassVaccinal.Presentation.Components
{
    using PassVaccinal.Presentation.Service;
    using System.Windows.Forms;

    partial class GenericSearch<T,U> where U:IService<T>
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel = new FlowLayoutPanel();
            this.splitContainer = new SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.SuspendLayout();


            this.splitContainer.Dock = DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = Orientation.Horizontal;
            this.splitContainer.Size = this.Size;
            //this.splitContainer.Height = 700;
            this.splitContainer.TabIndex = 0;
            //this.splitContainer.BackColor = System.Drawing.Color.Yellow;

            this.splitContainer.Panel1.Size = this.Size;
            this.splitContainer.SplitterDistance = 100;// this.Size;
            //this.splitContainer.Panel1.BackColor = System.Drawing.Color.Blue;
            //this.splitContainer.Panel2.BackColor = System.Drawing.Color.Orange;
            this.splitContainer.Panel2.Size = this.Size;

            this.flowLayoutPanel.Dock = DockStyle.Fill;
            this.flowLayoutPanel.Size = this.Size;
            //this.flowLayoutPanel.Width = 500;
            //this.flowLayoutPanel.BackColor = System.Drawing.Color.Gold;
            this.flowLayoutPanel.Name = "tableLayoutPanel";
            this.flowLayoutPanel.TabIndex = 0;

            this.genericList.Size = this.Size;

            this.splitContainer.Panel1.Controls.Add(this.flowLayoutPanel);           
            this.splitContainer.Panel2.Controls.Add(this.genericList);

            this.createForm();

            this.Controls.Add(this.splitContainer);
            this.AutoSize = true;
            this.Name = "GenericForm";
            //this.BackColor = System.Drawing.Color.Brown;
            this.ResumeLayout(false);
        }

        #endregion

        private FlowLayoutPanel flowLayoutPanel;
        private SplitContainer splitContainer;
        private GenericList<T,U> genericList;
    }
}
