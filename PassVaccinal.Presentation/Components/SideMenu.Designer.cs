﻿namespace PassVaccinal.Presentation.Components
{
    using System;
    using System.Collections;

    public partial class SideMenu
    {
        public event EventHandler OnMenuSelection;
        private static ArrayList NavItems = new ArrayList();
        public static string SelectedMenuLabel { get; private set; } = string.Empty;
        public static string selecteditem { get; private set; } = string.Empty;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();

            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "sidePanel";
            //this.panel1.Size = new System.Drawing.Size(200, 196);
            this.panel1.AutoSize = true;
            this.panel1.TabIndex = 0;
            this.panel1.Size=this.Size;
            // 
            // SideMenu
            // 
            //this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(panel1);
            this.Name = "SideMenu";
            //this.Size = new System.Drawing.Size(200, 196);
            this.MinimumSize = new System.Drawing.Size(100, 100);
            //this.AutoSize = true;
            this.Load += new System.EventHandler(this.SideMenu_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;

    }
}
