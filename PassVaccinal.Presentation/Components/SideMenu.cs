﻿namespace PassVaccinal.Presentation.Components
{
    using PassVaccinal.Presentation.Models;
    using System;
    using System.Collections;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class SideMenu : UserControl
    {
        public SideMenu()
        {
            InitializeComponent();
        }

        public ArrayList MenuItems
        {
            set
            {
                NavItems = value;
            }
            get
            {
                return (NavItems);
            }
        }

        private void SideMenu_Load(object sender, EventArgs e)
        {

        }
        
        public void RenderMenu()
        {
            if (NavItems.Count == 0) return;
            LoadMenuItems(NavItems);
        }

        private void LoadMenuItems(ArrayList NavItems)
        {
            int btnHeight = 30;//Properties.Resources.btnBgnd.Height;


            Panel mainpanel = new Panel
            {
                Dock = DockStyle.Top,
                AutoSize = true,
                Name = "mainpanel",
                Width = this.Width
            };

            Panel pnl = new Panel
            {
                Height = 20,
                Dock = DockStyle.Top
            };
            mainpanel.Controls.Add(pnl);
            string selectedbutton = string.Empty;
            foreach (NavigationItem navitems in NavItems)
            {
                if (!navitems.AuthorizedRole.Contains(PassVaxForm.CurrentUser.Role)) continue;

                Button btn = new Button
                {
                    Height = btnHeight,
                    Dock = DockStyle.Bottom,
                    Name = navitems.eventCode,
                    Text = navitems.MnuItem, 
                    Width =266//this.Size.Width
                };
                btn.Click += new EventHandler(btn_Click);
                

                if (navitems.Selected == true)
                {
                    selectedbutton = btn.Name;
                }

                mainpanel.Controls.Add(btn);

              

            }
            panel1.Controls.Add(mainpanel);


            if (selectedbutton != string.Empty)
            {
                Button btn = (Button)panel1.Controls["mainpanel"].Controls[selectedbutton];
                selecteditem = btn.Name;
                //ShowChilds(btn);
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            Button senderBtn = (Button)sender;
            
            SelectedMenuLabel = senderBtn.Text;
            selecteditem = senderBtn.Name;
            HeaderComponent.selecteditem = "list";
            OnMenuSelection(sender, e);

        }


        private void childbtnbtn_Click(object sender, EventArgs e)
        {
            SelectedMenuLabel = ((Button)sender).Text;
            OnMenuSelection(sender, e);
        }

        private void ResetAllPanels()
        {
            foreach (Control ctrl in panel1.Controls["mainpanel"].Controls)
            {
                if (ctrl is Panel)
                {
                    if (ctrl.Name.Length >= 6)
                    {
                        if (ctrl.Name.Substring(0, 6) == "panel_")
                        {
                            if (ctrl.Visible == true) ctrl.Visible = false;
                        }
                    }
                }
            }
        }
    }
}
