﻿namespace PassVaccinal.Presentation.Components
{
    using PassVaccinal.Presentation.Models;
    using PassVaccinal.Presentation.Models.Attributes;
    using PassVaccinal.Presentation.Service;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Forms;

    public partial class GenericList<T,U> where U : IService<T>
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new TableLayoutPanel();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel.ColumnCount = this.viewOption.ColumnSize;

            this.tableLayoutPanel.Name = "tableLayoutPanel1";
            this.tableLayoutPanel.RowCount = this.Items.Count + 1;

            this.tableLayoutPanel.TabIndex = 0;
            this.tableLayoutPanel.AutoSize = true;
            var label2 = new Label();
            label2.Text = "Liste de ...";
            this.Controls.Add(label2);
            if (this.Items.Count > 0)
            {
                this.renderHeader();
                this.renderBody();
            }
            else
            {
                var label = new Label();
                label.Text = "Liste Vide";
                this.Controls.Add(label);
            }
            

            this.Controls.Add(this.tableLayoutPanel);
            this.AutoSize = true;
            this.Name = "GenericList";
            this.ResumeLayout(false);

        }

        #endregion

        private void renderHeader()
        {
            if (this.viewOption.IncludeCheckBox)
            {
                ListCheckBox<T> checkBox = new ListCheckBox<T>();

                checkBox.Value = "header";
                checkBox.Val = default(T);

                this.tableLayoutPanel.Controls.Add(checkBox, 0, 0);

                checkBox.Click += new EventHandler(CheckBox_CheckedChanged);

                this.headerCheckBox = checkBox;
            }
            foreach (var column in this.viewOption.Columns)
            {
                var label = new Label();

                label.Text = column.DisplayName;

                this.tableLayoutPanel.Controls.Add(label, column.Order, 0);
            }
        }

        public void ReInit(List<T> newItems)
        {
            this.Items = newItems;
            this.tableLayoutPanel.Controls.Clear();
            if (this.Items.Count > 0)
            {
                this.renderHeader();
                this.renderBody();
            }
            else
            {
                var label = new Label();
                label.Text = "Liste Vide";
                this.Controls.Add(label);
            }
        }

        private void renderBody()
        {
            int start = 1;

            foreach (T obj in this.Items)
            {
                if (this.viewOption.IncludeCheckBox)
                {
                    ListCheckBox<T> checkBox1 = new ListCheckBox<T>();

                    checkBox1.Click += new EventHandler(CheckBox_CheckedChanged);

                    var p = this.viewOption.ObjectType.GetProperties().FirstOrDefault(x => x.CustomAttributes != null && x.CustomAttributes.Any(y => y.AttributeType == typeof(Identifier)));

                    checkBox1.Value = p != null ? p.GetValue(obj) : obj;
                    checkBox1.Val = obj;
                    checkBox1.service = this.service;
                    this.tableLayoutPanel.Controls.Add(checkBox1, 0, start);

                    this.listObj.Add(checkBox1);
                }
                foreach (var column in this.viewOption.Columns)
                {
                    var prop = this.viewOption.ObjectType.GetProperty(column.Name);

                    var label = new TextBox();

                    label.Text = prop.GetValue(obj).ToString();
                    label.ReadOnly = true;
                    label.BorderStyle = 0;

                    this.tableLayoutPanel.Controls.Add(label, column.Order, start);
                }
                start++;
            }
        }

        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (ListCheckBox<T>)sender;

            if (checkBox.Value.ToString() == "header")
            {
                foreach (var a in this.listObj)
                {
                    a.Checked = checkBox.Checked;
                }
            }
            else
            {
                this.headerCheckBox.Checked = !this.listObj.Any(x => !x.Checked);
            }

            var headerComponentExist = PassVaxForm.Components.TryGetValue("header", out Control header);

            if (headerComponentExist)
            {
                var c = header as HeaderComponent;

                c.headerContainer_Refresh<T>(sender, e, listObj);
            }
        }

        private TableLayoutPanel tableLayoutPanel;
        private List<ListCheckBox<T>> listObj { get; set; } = new List<ListCheckBox<T>>();
        private ListCheckBox<T> headerCheckBox;
    }
}
