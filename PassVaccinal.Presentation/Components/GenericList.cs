﻿namespace PassVaccinal.Presentation.Components
{
    using PassVaccinal.Presentation.Models;
    using PassVaccinal.Presentation.Service;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class GenericList<T,U> : UserControl where U : IService<T>
    {
        public List<T> Items { get; private set; }

        private ListOption viewOption { get; set; }

        private U service { get; set; }


        public event EventHandler OnRowChecked;

        public GenericList(ListModel<T> list,U u)
        {
            this.Items = list.List;
            this.viewOption = list.DisplayOption;
            this.service = u;
            InitializeComponent();
        }
    }
}