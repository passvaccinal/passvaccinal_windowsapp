﻿namespace PassVaccinal.Presentation.Components
{
    using PassVaccinal.Presentation.Models;
    using PassVaccinal.Presentation.Service;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    public partial class HeaderComponent : UserControl
    {
        public event EventHandler OnMenuSelection;
        public event EventHandler OnRowChecked;

        private static ArrayList NavItems = new ArrayList();

        public HeaderComponent()
        {
            InitializeComponent();
        }
        public ArrayList MenuItems
        {
            set
            {
                NavItems = value;
            }
            get
            {
                return NavItems;
            }
        }

        public static string SelectedMenuLabel { get; internal set; }

        public void RenderMenu()
        {
            if (NavItems.Count == 0) return;
            LoadMenuItems(NavItems);
        }

        private void LoadMenuItems(ArrayList NavItems)
        {
            int btnHeight = 50;//Properties.Resources.btnBgnd.Height;
            int btnWidth = 190;

            string selectedbutton = string.Empty;
            foreach (HeaderNavigationItem navitems in NavItems)
            {
                HeaderButton btn = new HeaderButton
                {
                    Height = btnHeight,
                    Width = btnWidth,
                    Dock = DockStyle.Bottom,
                    Name = navitems.eventCode,
                    Text = navitems.MnuItem,
                    Enabled = navitems.State,
                    AlwaysActive = navitems.AlwaysActive,
                    AllowMultipleSource = navitems.AllowMultipleSource
                };
                btn.Click += new EventHandler(btn_Click);

                if (navitems.Selected == true)
                {
                    selectedbutton = btn.Name;
                    selecteditem = btn.Name;
                    SelectedMenuLabel = btn.Text;
                    //Form1.ActivatedHeaderMenu = btn.Name;
                }

                panel.Controls.Add(btn);
            }
            //panel1.Controls.Add(mainpanel);
        }

        private void btn_Click(object sender, EventArgs e)
        {
            var currentBtn = (Button)sender;
            selecteditem = currentBtn.Name;
            SelectedMenuLabel = currentBtn.Text;
            if (selecteditem == "delete")
            {
                if(PassVaxForm.SelectedObject.Count > 0)
                {
                    foreach(var obj in PassVaxForm.SelectedObject)
                    {
                        Guid id = Guid.Parse(obj.ToString());
                        switch (SideMenu.selecteditem)
                        {
                            case "users":
                                UserService.Service.RemoveById(id);
                                break;
                            case "dose":
                                DoseService.Service.RemoveById(id);
                                break;
                            case "disease":
                                DiseaseService.Service.RemoveById(id);
                                break;
                            default:
                                PatientService.Service.RemoveById(id);
                                break;
                        }
                    }
                }                
            }
            else
            {
                OnMenuSelection(sender, e);
            }
        }

        public void headerContainer_Refresh<T>(object sender, EventArgs e, List<ListCheckBox<T>> list)
        {
            var checkbox = (ListCheckBox<T>)sender;

            PassVaxForm.SelectedObject.Clear();

            var selectedItems = list.Where(x => x.Checked).Select(x=>x.Value).ToList();
            
            var activatedCheckBoxCount = selectedItems.Count();

            PassVaxForm.SelectedObject.AddRange(selectedItems);

            foreach (Control ctrl in this.panel.Controls)
            {
                if (ctrl is HeaderButton)
                {
                    var hdrBtn = ctrl as HeaderButton;
                    if (!hdrBtn.AlwaysActive)
                    {
                        ctrl.Enabled = activatedCheckBoxCount > 1 ? hdrBtn.AllowMultipleSource : activatedCheckBoxCount == 1;
                    }
                }
            }
        }
    }
}
