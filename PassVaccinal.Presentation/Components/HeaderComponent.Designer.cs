﻿namespace PassVaccinal.Presentation.Components
{
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class HeaderComponent 
    {

        public static string selecteditem { get;  set; } = string.Empty;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel = new FlowLayoutPanel();
           
            this.SuspendLayout();

            this.panel.Dock = DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "HeaderPanel";
            //this.panel1.Size = new System.Drawing.Size(1026, 150);
            this.panel.AutoSize = true;
            this.panel.Height = 150;
            this.panel.TabIndex = 0;
          
            // 
            // HeaderComponent
            // 
            //this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = AutoScaleMode.Font;
            //this.BackColor = Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.panel);            
            this.Name = "HeaderComponent";
            //this.Size = new System.Drawing.Size(1026, 150);
            this.AutoSize = true;
            this.ResumeLayout(false);

        }

        #endregion

        private FlowLayoutPanel panel;

    }
}
