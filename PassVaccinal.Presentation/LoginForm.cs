﻿namespace PassVaccinal.Presentation
{
    using PassVaccinal.Presentation.Helper;
    using PassVaccinal.Presentation.Service;
    using System;
    using System.Windows.Forms;
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username;
            string password;

            username = login.Text;
            password = Util.HashText(mdp.Text);
            Splash.ShowSplashScreen();
            var user = UserService.Service.FindUser(username, password);
            Splash.CloseForm();
            if (user!=null)
            {
                PassVaxForm.CurrentUser = user;
                DialogResult = DialogResult.OK; 
            }
            else
            {
                MessageBox.Show("Login ou mot de passe erroné");
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
