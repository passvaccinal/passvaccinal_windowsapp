﻿namespace PassVaccinal.Presentation.Models
{
    using System.Collections.Generic;

    public class ListModel<T>
    {
        public ListOption DisplayOption { get; set; }

        public List<T> List { get; set; }
    }
}
