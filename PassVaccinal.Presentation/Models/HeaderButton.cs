﻿namespace PassVaccinal.Presentation.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public class HeaderButton : Button
    {
       public bool IsActive { get; set; }

       public bool AllowMultipleSource { get; set; }
       public bool AlwaysActive { get; set; }
    }
}
