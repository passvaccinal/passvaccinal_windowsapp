﻿namespace PassVaccinal.Presentation.Models
{
    using PassVaccinal.Presentation.Models.Attributes;
    using System;
    using System.Collections.Generic;

    public class ListOption
    {
        private List<ColumnSetting> columns;

        public List<ColumnSetting> Columns
        {
            get
            {
                if (this.columns != null)
                {
                    return this.columns;
                }
                var a = this.ObjectType;

                var properties = a.GetProperties();

                var result = new List<ColumnSetting>();

                var start = this.IncludeCheckBox ? 1 : 0;

                foreach (var property in properties)
                {
                    var identifier = property.GetCustomAttributes(typeof(HideOnList), true);
                    if (identifier.Length > 0)
                    {
                        continue;
                    }
                    var column = new ColumnSetting(property.Name)
                    {
                        Order = start
                    };
                    result.Add(column);

                    start++;
                }
                this.columns = result;

                return result;
            }
            set
            {
                this.columns = value;
            }
        }

        public bool IncludeCheckBox { get; set; }

        public Type ObjectType { get; set; }

        public int ColumnSize => this.IncludeCheckBox ? this.Columns.Count + 1 : this.Columns.Count;
    }
}
