﻿namespace PassVaccinal.Presentation.Models
{
    using PassVaccinal.Presentation.Service;
    using System.Windows.Forms;

    public class ListCheckBox<T>: CheckBox
    {
        public object Value { get; set; }

        public T Val { get; set; }

        public IService<T> service { get; set; }

        
    }
}
