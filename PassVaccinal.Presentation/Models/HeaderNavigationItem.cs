﻿namespace PassVaccinal.Presentation.Models
{
    using System.Collections;

    public class HeaderNavigationItem : NavigationItem
    {
        public string Icon;

        public bool State { get; set; } = true;

        public bool AlwaysActive { get; set; }

        public bool AllowMultipleSource { get; set; } = true;

        public HeaderNavigationItem(string _MnuItem, string _eventCode, ArrayList _childNavItems, bool _selected) : base(_MnuItem, _eventCode, _childNavItems, _selected)
        {

        }
        public HeaderNavigationItem(string _MnuItem, string _eventCode, ArrayList _childNavItems) : base(_MnuItem, _eventCode, _childNavItems)
        {

        }
    }
}
