﻿namespace PassVaccinal.Presentation.Models
{
    using PassVaccinal.Presentation.Models.Entity;
    using System.Collections;
    using System.Collections.Generic;

    public class NavigationItem
    {
        public string MnuItem;
        public string eventCode;
        public ArrayList childNavItems;
        public bool Selected = false;
        public List<Role> AuthorizedRole;

        public NavigationItem(string _MnuItem, string _eventCode, ArrayList _childNavItems, bool _selected)
        {
            MnuItem = _MnuItem;
            eventCode = _eventCode;
            childNavItems = _childNavItems;
            Selected = _selected;
        }
        public NavigationItem(string _MnuItem, string _eventCode, ArrayList _childNavItems)
        {
            MnuItem = _MnuItem;
            eventCode = _eventCode;
            childNavItems = _childNavItems;
        }
    }
}
