﻿namespace PassVaccinal.Presentation.Models.Entity
{
    using PassVaccinal.Presentation.Models.Attributes;
    using System;

    public class Disease
    {
        [Identifier]
        [HideOnList]
        public Guid Id { get; set; } = Guid.NewGuid();

        [DisplayLabel("Date du test")]
        public DateTime Date { get; set; } = DateTime.Now;

        [Hide]
        public Patient Patient { get; set; }

        [AutocompleteDisplay]
        [DisplayLabel("Patient")]
        public Guid PatientId { get; set; }

        [DisplayLabel("Type de test")]
        public string TestType { get; set; }
        [DisplayLabel("Commentaire")]
        public string Notes { get; set; }

        public Disease() { }

        public Disease(Data.Models.Disease d)
        {
            this.Id = d.Id;
            this.Date = d.Date;
            this.PatientId = d.PatientId;
            this.Notes = d.Comment;
            this.TestType = d.TestType;
        }
    }
}
