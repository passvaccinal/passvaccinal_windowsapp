﻿namespace PassVaccinal.Presentation.Models.Entity
{
    using PassVaccinal.Presentation.Models.Attributes;
    using System;

    public class Dose
    {
        [Identifier]
        [HideOnList]
        public Guid Id { get; set; } = Guid.NewGuid();
        [DisplayLabel("Nom du vaccin")]
        public string Vaccin { get; set; }
        [Hide]
        [HideOnList]
        public Patient Patient { get; set; }
        [Searchable]
        [DisplayLabel("Id du patient")]
        [AutocompleteDisplay]
        public Guid PatientId { get; set; }
        [DisplayLabel("Date du vaccin")]
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
