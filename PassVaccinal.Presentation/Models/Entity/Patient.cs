﻿namespace PassVaccinal.Presentation.Models.Entity
{
    using PassVaccinal.Presentation.Models.Attributes;
    using System;

    public class Patient
    {
        [Identifier]
        [HideOnList]
        public Guid Id { get; set; } = Guid.NewGuid();

        [DisplayLabel("Nom")]
        public string LastName { get; set; }

        [DisplayLabel("Prénom")]
        public string FirstName { get; set; }

        [DisplayLabel("Date de naissance")]
        public DateTime BirthDay { get; set; } = DateTime.Now;


        [DisplayLabel("Sexe")]
        public Sex Gender { get; set; }

        [DisplayLabel("Adresse")]
        public string Address { get; set; }

        [DisplayLabel("CIN")]
        public string Cin { get; internal set; }

        public Patient() { }

        public Patient(Data.Models.Patient p)
        {
            this.Id = p.Id;
            this.LastName = p.LastName;
            this.FirstName = p.FirstName;
            this.BirthDay = p.Birthdate;
            this.Cin = p.Cin;
            this.Address = p.Address;
            this.Gender = (Sex)p.Sex;
        }
    }

    public enum Sex
    {
        [DisplayLabel("Homme")]
        Male = 0,
        [DisplayLabel("Femme")]
        Female = 1
    }
}
