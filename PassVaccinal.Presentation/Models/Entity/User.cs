﻿namespace PassVaccinal.Presentation.Models.Entity
{
    using PassVaccinal.Data.Models;
    using PassVaccinal.Presentation.Helper;
    using PassVaccinal.Presentation.Models.Attributes;
    using System;

    public class User
    {
        private string _password { get; set; }

        [Identifier]
        [HideOnList]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Searchable]
        [DisplayLabel("Nom d'utilisateur")]
        public string UserName { get; set; }

        [DisplayLabel("Mot de passe")]
        [Password]
        [HideOnList]
        public string Password
        {
            get
            {
                return string.IsNullOrEmpty(this._password)?string.Empty:this._password;
            }
            set
            {
                if (_password == null)
                {
                    this._password = Util.HashText(value);// Convert.ToBase64String(inArray);
                }
            }
        }

        [DisplayLabel("Role")]
        public Role Role { get; set; }

        public User() { }
        public User(Data.Models.User usr) {
            this.Id = usr.Id;
            this.UserName = usr.UserName;
            this.Role = (Role)usr.Role;
        }
       
    }
    public enum Role
    {
        Admin = 1,
        Superviseur = 2,
        Contributeur = 3
    }
}
