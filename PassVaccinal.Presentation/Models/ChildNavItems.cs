﻿namespace PassVaccinal.Presentation.Models
{
    public class ChildNavItems
    {
        public string MnuItem;
        public string eventCode;
        public ChildNavItems(string _MnuItem, string _eventCode)
        {
            MnuItem = _MnuItem;
            eventCode = _eventCode;
        }
    }
}
