﻿namespace PassVaccinal.Presentation.Models
{
    public class ColumnSetting
    {
        private string displayName;
        public string Name { get; set; }

        public string DisplayName
        {
            get
            {
                return string.IsNullOrEmpty(this.displayName) ? this.Name : this.displayName;
            }
            set
            {
                this.displayName = value;
            }
        }
        public int Order { get; set; }

        public ColumnSetting(string name)
        {
            this.Name = name;
        }
    }
}
