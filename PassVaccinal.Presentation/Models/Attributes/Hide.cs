﻿namespace PassVaccinal.Presentation.Models.Attributes
{
    using System;

    public class Hide : Attribute
    {
        public bool show;

        public Hide(bool show=true)
        {
            this.show = show;
        }
    }
}
