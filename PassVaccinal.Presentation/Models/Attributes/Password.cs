﻿namespace PassVaccinal.Presentation.Models.Attributes
{
    using System;

    public class Password : Attribute
    {
        public bool isPassword;

        public Password(bool isPwd=true)
        {
            this.isPassword = isPwd;
        }
    }
}
