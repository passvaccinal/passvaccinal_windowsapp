﻿namespace PassVaccinal.Presentation.Models.Attributes
{
    using System;

    public class DisplayLabel : Attribute
    {
        public string Label;

        public DisplayLabel(string label)
        {
            this.Label = label;
        }
    }
}
