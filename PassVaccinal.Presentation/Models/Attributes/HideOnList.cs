﻿namespace PassVaccinal.Presentation.Models.Attributes
{
    using System;

    public class HideOnList : Attribute
    {
        public bool show;

        public HideOnList(bool show=true)
        {
            this.show = show;
        }
    }
}
