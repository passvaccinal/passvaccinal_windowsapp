﻿namespace PassVaccinal.Presentation.Models.Attributes
{
    using System;

    public class AutocompleteDisplay : Attribute
    {
        public bool isAutoCompletable;

        public AutocompleteDisplay(bool isPwd=true)
        {
            this.isAutoCompletable = isPwd;
        }
    }
}
