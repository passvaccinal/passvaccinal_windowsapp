﻿namespace PassVaccinal.Presentation.Models.Attributes
{
    using System;

    public class Searchable : Attribute
    {
        public bool status;

        public Searchable(bool status = true)
        {
            this.status = status;
        }
    }
}
