﻿namespace PassVaccinal.Presentation.Service
{
    using System;
    using System.Collections.Generic;

    public interface IService<T>
    {
        List<T> GetAll();
        T GetById(Guid id);
        void RemoveById(Guid id);
        void Create(T id);
        void Update(T id);
    }
}
