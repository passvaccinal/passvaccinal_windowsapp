﻿namespace PassVaccinal.Presentation.Service
{
    using PassVaccinal.Data.Repositories;
    using PassVaccinal.Presentation.Models.Entity;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PatientService : IService<Patient>
    {
        private static PatientRepository patientRepo = new PatientRepository();

        private static PatientService instance = null;
        private PatientService() { }
        public static PatientService Service
        {
            get
            {
                if (instance == null)
                {
                    instance = new PatientService();
                }
                return instance;
            }
        }

        public void Create(Patient patient)
        {
            var usr = new Data.Models.Patient { Id = patient.Id, LastName = patient.LastName, FirstName = patient.FirstName, Birthdate = patient.BirthDay,Address=patient.Address,Cin=patient.Cin };
            patientRepo.AddPatient(usr);
        }

        public List<Patient> GetAll()
        {
            var res = patientRepo.GetPatients().Select(x => new Patient (x)).ToList();
            return res;
        }

        public Patient GetById(Guid id)
        {
            var res = patientRepo.GetPatientById(id);
            return new Patient(res);
        }

        public void RemoveById(Guid id)
        {
            patientRepo.DeletePatient(id);
        }

        public void Update(Patient p)
        {
            var res = patientRepo.GetPatientById(p.Id);
            res.LastName = p.LastName;
            res.FirstName = p.FirstName;
            res.Birthdate = p.BirthDay;
            res.Cin = p.Cin;
            res.Address = p.Address;
            patientRepo.EditPatient(res);
        }
    }
}
