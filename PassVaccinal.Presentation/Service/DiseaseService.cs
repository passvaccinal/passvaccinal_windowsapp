﻿namespace PassVaccinal.Presentation.Service
{
    using PassVaccinal.Data.Repositories;
    using PassVaccinal.Presentation.Models.Entity;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    public class DiseaseService : IService<Disease>
    {
        private static DiseaseRepository diseaseRepo = new DiseaseRepository();

        private static DiseaseService instance = null;
        private DiseaseService() { }
        public static DiseaseService Service
        {
            get
            {
                if (instance == null)
                {
                    instance = new DiseaseService();
                }
                return instance;
            }
        }

        public void Create(Disease user)
        {
            var usr = new Data.Models.Disease { Id = user.Id, Date = user.Date, PatientId = user.PatientId, Comment = user.Notes,TestType=user.TestType };
            diseaseRepo.AddDisease(usr);
        }

        public List<Disease> GetAll()
        {
            var res = diseaseRepo.GetDiseases().Select(x => new Disease (x)).ToList();
            return res;
        }

        public Disease GetById(Guid id)
        {
            var res = diseaseRepo.GetDiseaseById(id);
            return new Disease(res) ;
        }

        public void RemoveById(Guid id)
        {
            diseaseRepo.DeleteDisease(id);
        }

        public void Update(Disease d)
        {
            var res = diseaseRepo.GetDiseaseById(d.Id);

            res.Date = d.Date;
            res.PatientId = d.PatientId;
            res.Comment = d.Notes;
            res.TestType = d.TestType;
            diseaseRepo.EditDisease(res);
        }
    }
}
