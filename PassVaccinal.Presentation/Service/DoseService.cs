﻿namespace PassVaccinal.Presentation.Service
{
    using PassVaccinal.Data.Repositories;
    using PassVaccinal.Presentation.Models.Entity;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class DoseService : IService<Dose>
    {
        private static DoseRepository doseRepo = new DoseRepository();

        private static DoseService instance = null;
        private DoseService() { }
        public static DoseService Service
        {
            get
            {
                if (instance == null)
                {
                    instance = new DoseService();
                }
                return instance;
            }
        }

        public void Create(Dose d)
        {
            //var patient = new Data.Models.Patient();
            var dose = new Data.Models.Dose() { Id = d.Id, Name = d.Vaccin, Date = d.Date, PatientId = d.PatientId };
            doseRepo.AddDose(dose);
        }


        public List<Dose> GetAll()
        {
            var res = doseRepo.GetDoses().Select(x => new Dose { Id = x.Id, Vaccin = x.Name, Date = x.Date, PatientId = x.PatientId }).ToList();
            return res;
        }

        public Dose GetById(Guid id)
        {
            var res = doseRepo.GetDoseById(id);
            return new Dose { Id = res.Id, Vaccin = res.Name, Date = res.Date, PatientId = res.PatientId };
        }

        public void RemoveById(Guid id)
        {
            doseRepo.DeleteDose(id);
        }

        public void Update(Dose dose)
        {
            var res = doseRepo.GetDoseById(dose.Id);
            res.Name = dose.Vaccin;
            res.Date = dose.Date;
            //res.Password = usr.Password;
            doseRepo.EditDose(res);
        }
    }
}
