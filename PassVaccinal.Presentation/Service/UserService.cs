﻿namespace PassVaccinal.Presentation.Service
{
    using PassVaccinal.Data.Repositories;
    using PassVaccinal.Presentation.Models.Entity;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    public class UserService : IService<User>
    {
        private static UserRepository userRepo = new UserRepository();

        private static UserService instance = null;
        private UserService() { }
        public static UserService Service
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserService();
                }
                return instance;
            }
        }

        public void Create(User user)
        {
            var usr = new Data.Models.User { Id = user.Id, UserName = user.UserName, Password = user.Password, Role = (int)user.Role };
            userRepo.AddUser(usr);
        }

        public List<User> GetAll()
        {
            var res = userRepo.GetUsers().Select(x => new User(x)).ToList();
            return res;
        }

        public User GetById(Guid id)
        {
            var res = userRepo.GetUserById(id);
            return new User(res);
        }

        public void RemoveById(Guid id)
        {
            userRepo.DisableUser(id);
        }

        public void Update(User usr)
        {
            var res = userRepo.GetUserById(usr.Id);
            res.UserName = usr.UserName;
            res.Role = (int)usr.Role;
            res.Password = usr.Password;
            userRepo.EditUser(res);
        }
        public User FindUser(string username,string pwd)
        {
            var res = userRepo.FindUser(username,pwd);
            if (res == null) return null;
            return new User (res);
        }

        
    }
    
}
