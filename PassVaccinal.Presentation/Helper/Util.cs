﻿namespace PassVaccinal.Presentation.Helper
{
    using PassVaccinal.Presentation.Models.Attributes;
    using System;
    using System.Security.Cryptography;
    using System.Text;
    using System.Windows.Forms;

    public static class Util
    {
        public static string HashText(string value)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(value);
            byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
            return Convert.ToBase64String(inArray);
        }

        public static void ResetAllControls<T>(Control form)
        {
            foreach (Control control in form.Controls)
            {
                if (control.Name.IndexOf("input_") < 0) continue;

                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;

                    var attributeName = control.Name.Replace("input_", string.Empty);
                    var prop = typeof(T).GetProperty(attributeName);
                    var identifier = prop.GetCustomAttributes(typeof(Identifier), true);
                    if (identifier.Length > 0)
                    {
                        textBox.Text = Guid.NewGuid().ToString();
                    }
                    else
                    {

                        textBox.Text = null;
                    }
                }

                if (control is ComboBox)
                {
                    ComboBox comboBox = (ComboBox)control;
                    if (comboBox.Items.Count > 0)
                        comboBox.SelectedIndex = 0;
                }

                if (control is CheckBox)
                {
                    CheckBox checkBox = (CheckBox)control;
                    checkBox.Checked = false;
                }

                if (control is ListBox)
                {
                    ListBox listBox = (ListBox)control;
                    listBox.ClearSelected();
                }
            }
        }
    }
}
