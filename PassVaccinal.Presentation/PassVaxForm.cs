﻿namespace PassVaccinal.Presentation
{
    using PassVaccinal.Presentation.Components;
    using PassVaccinal.Presentation.Models;
    using PassVaccinal.Presentation.Models.Entity;
    using PassVaccinal.Presentation.Service;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class PassVaxForm : Form
    {
        public PassVaxForm()
        {
            InitializeComponent();
            InitHeaderMenu();
            InitMenuList();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void headerContainer_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void headerContainer_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void InitMenuList()
        {
            var adminRoles = new List<Role>() { Role.Admin,Role.Superviseur};
            var contributorRoles = new List<Role>() { Role.Admin,Role.Superviseur,Role.Contributeur};
            NavigationItem nv = new NavigationItem("Patients", "patients", new ArrayList(), true) { AuthorizedRole=contributorRoles};
            NavigationItem nv1 = new NavigationItem("Doses", "doses", new ArrayList(), true) { AuthorizedRole = contributorRoles };
            NavigationItem nv2 = new NavigationItem("Maladies", "diseases", new ArrayList(), true) { AuthorizedRole = contributorRoles };
            NavigationItem nv3 = new NavigationItem("Utilisateur", "users", new ArrayList(), true) { AuthorizedRole = adminRoles };

            this.sideMenu.MenuItems = new ArrayList { nv, nv1, nv2, nv3 };
            this.sideMenu.RenderMenu();
        }

        private void sideMenu1_OnMenuSelection(object sender, EventArgs e)
        {
            this.Text = $"{HeaderComponent.SelectedMenuLabel} {SideMenu.SelectedMenuLabel}";
            this.ActivatePanel($"panel_{HeaderComponent.selecteditem}_{SideMenu.selecteditem}");
        }

        private void InitHeaderMenu()
        {
            HeaderNavigationItem nv = new HeaderNavigationItem("List", "list", null, true) { AlwaysActive = true };
            HeaderNavigationItem nv1 = new HeaderNavigationItem("Add", "add", null) { AlwaysActive = true };
            HeaderNavigationItem nv2 = new HeaderNavigationItem("Update", "update", null, false) { State = false, AllowMultipleSource = false };
            HeaderNavigationItem nv3 = new HeaderNavigationItem("Details", "detail", null, false) { State = false, AllowMultipleSource = false };
            HeaderNavigationItem nv4 = new HeaderNavigationItem("Delete", "delete", null, false) { State = false };

            this.headerContainer.MenuItems = new ArrayList { nv, nv1, nv2, nv3, nv4 };
            this.headerContainer.RenderMenu();
        }

        private void ActivatePanel(string panelName)
        {
            Panel currentPanel = (Panel)this.bodySplitContainer.Panel2.Controls[panelName];
            if (currentPanel == null)
            {
                currentPanel = new Panel
                {
                    Name = panelName,
                    /* AutoSize = true,*/
                    Size = this.bodySplitContainer.Panel2.Size,
                    //BackColor = Color.Purple,
                    //Width = this.bodySplitContainer.Panel2.Width
                };
                this.bodySplitContainer.Panel2.Controls.Add(currentPanel);
                this.CreateForm(HeaderComponent.selecteditem, SideMenu.selecteditem, currentPanel);
            }
            else
            {
                //currentPanel.BackColor = Color.Chocolate;
            }
            ResetAllPanels();
            currentPanel.Visible = true;

            //return null;
        }

        private void ResetAllPanels()
        {
            foreach (Control ctrl in this.bodySplitContainer.Panel2.Controls)
            {
                if (ctrl is Panel)
                {
                    if (ctrl.Name.Length >= 6)
                    {
                        if (ctrl.Name.Substring(0, 6) == "panel_")
                        {
                            if (ctrl.Visible == true) ctrl.Visible = false;
                        }
                    }
                }
            }
        }

        private void CreateForm(string action, string type, Panel container)
        {
            switch (action)
            {
                case "list":
                    ListOption listOption;
                    switch (type)
                    {
                        case "users":
                            listOption = new ListOption
                            {
                                IncludeCheckBox = true,
                                ObjectType = typeof(User)
                            };
                            var userService = UserService.Service;
                            var userList = new GenericList<User, UserService>(new ListModel<User> { List = new List<User>(), DisplayOption = listOption }, userService);
                            var search = new GenericSearch<User, UserService>(userList, userService, container.Size);
                            container.Controls.Add(search);
                            break;
                        case "patients":
                            listOption = new ListOption
                            {
                                IncludeCheckBox = true,
                                ObjectType = typeof(Patient)
                            };
                            var patientService = PatientService.Service;
                            var patientList = new GenericList<Patient, PatientService>(new ListModel<Patient> { List = new List<Patient>(), DisplayOption = listOption }, patientService);
                            var searchP = new GenericSearch<Patient, PatientService>(patientList, patientService, container.Size);
                            container.Controls.Add(searchP);
                            break;
                        case "doses":
                            listOption = new ListOption
                            {
                                IncludeCheckBox = true,
                                ObjectType = typeof(Dose)
                            };
                            var doseService = DoseService.Service;
                            var doseList = new GenericList<Dose, DoseService>(new ListModel<Dose> { List = new List<Dose>(), DisplayOption = listOption }, doseService);
                            var searchDo = new GenericSearch<Dose, DoseService>(doseList, doseService, container.Size);
                            container.Controls.Add(searchDo);
                            break;
                        case "diseases":
                            listOption = new ListOption
                            {
                                IncludeCheckBox = true,
                                ObjectType = typeof(Disease)
                            };
                            var diseaseService = DiseaseService.Service;
                            var diseaseList = new GenericList<Disease, DiseaseService>(new ListModel<Disease> { List = new List<Disease>(), DisplayOption = listOption }, diseaseService);
                            var searchDi = new GenericSearch<Disease, DiseaseService>(diseaseList, diseaseService, container.Size);
                            container.Controls.Add(searchDi);
                            break;
                        default:
                            listOption = new ListOption
                            {
                                IncludeCheckBox = true,
                                ObjectType = typeof(Patient)
                            };
                            var genericList = new GenericList<Patient, PatientService>(new ListModel<Patient> { List = new List<Patient>(), DisplayOption = listOption }, PatientService.Service);
                            var searchG = new GenericSearch<Patient, PatientService>(genericList, PatientService.Service, container.Size);
                            container.Controls.Add(searchG);
                            break;
                    }
                    break;
                case "add":
                    switch (type)
                    {
                        case "users":
                            GenericForm<User, UserService> user = new GenericForm<User, UserService>(new User(), container, UserService.Service);
                            container.Controls.Add(user);
                            break;
                        case "patients":
                            GenericForm<Patient, PatientService> patient = new GenericForm<Patient, PatientService>(new Patient(), container, PatientService.Service);
                            container.Controls.Add(patient);
                            break; ;
                        case "doses":
                            GenericForm<Dose, DoseService> dose = new GenericForm<Dose, DoseService>(new Dose(), container, DoseService.Service);
                            container.Controls.Add(dose);
                            break; ;
                        case "diseases":
                            GenericForm<Disease, DiseaseService> disease = new GenericForm<Disease, DiseaseService>(new Disease(), container, DiseaseService.Service);
                            container.Controls.Add(disease);
                            break; ;
                        default:
                            GenericForm<User, UserService> person = new GenericForm<User, UserService>(new User(), container, UserService.Service);
                            container.Controls.Add(person);
                            break;
                    }
                    break;
                case "update":
                    Guid id = Guid.Parse(PassVaxForm.SelectedObject[0].ToString());
                    switch (type)
                    {
                        case "users":
                            GenericForm<User, UserService> user = new GenericForm<User, UserService>(UserService.Service.GetById(id), container, UserService.Service);
                            user.IsCreation = false;
                            container.Controls.Add(user);
                            break;
                        case "patients":
                            GenericForm<Patient, PatientService> patient = new GenericForm<Patient, PatientService>(PatientService.Service.GetById(id), container, PatientService.Service);
                            patient.IsCreation = false;
                            container.Controls.Add(patient);
                            break; ;
                        case "doses":
                            GenericForm<Dose, DoseService> dose = new GenericForm<Dose, DoseService>(DoseService.Service.GetById(id), container, DoseService.Service);
                            dose.IsCreation = false;
                            container.Controls.Add(dose);
                            break; ;
                        case "diseases":
                            GenericForm<Disease, DiseaseService> disease = new GenericForm<Disease, DiseaseService>(DiseaseService.Service.GetById(id), container, DiseaseService.Service);
                            disease.IsCreation = false;
                            container.Controls.Add(disease);
                            break; ;
                        default:
                            GenericForm<User, UserService> person = new GenericForm<User, UserService>(UserService.Service.GetById(id), container, UserService.Service);
                            person.IsCreation = false;
                            container.Controls.Add(person);
                            break;
                    }
                    break;
                case "delete":
                    break;
                default:
                    break;
            }
        }

        public void headerContainer_ReInit(object sender, EventArgs e)
        {
            var checkbox = (CheckBox)sender;

            foreach (Control ctrl in this.headerContainer.Controls)
            {
                if (ctrl is Button)
                {
                    ctrl.Enabled = checkbox.Checked;
                }
            }
        }
    }
}
