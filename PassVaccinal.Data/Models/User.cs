﻿namespace PassVaccinal.Data.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class User
    {
        public Guid Id { get;set; }

        [Required]
        [MaxLength(50)]
        public string UserName { get;set; }
        [Required]
        [MaxLength(200)]
        public string Password { get;set; }
        public int Role { get;set; }
        public bool IsDisabled { get;set;}

    }
   
}
