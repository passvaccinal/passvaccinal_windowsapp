﻿namespace PassVaccinal.Data.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Disease
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        [MaxLength(50)]
        public string TestType { get; set; }
        [MaxLength(500)]
        public string Comment { get; set; }
        [ForeignKey("Patient")]
        public Guid PatientId { get; set; }
        public Patient Patient { get; set; }
    }
}
