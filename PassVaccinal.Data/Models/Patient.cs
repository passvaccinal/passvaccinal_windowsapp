﻿namespace PassVaccinal.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Patient
    {
        public Guid Id { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [StringLength(12)]
        public string Cin { get; set; }
        [MaxLength(200)]
        public string Address { get; set; }
        public DateTime Birthdate { get; set; }
        public int Sex { get; set; }
        public List<Dose> Doses { get; set; }
        public List<Disease> Diseases { get; set; }
    }
}
