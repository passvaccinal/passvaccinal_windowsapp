﻿namespace PassVaccinal.Data.Repositories
{
    using PassVaccinal.Data.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class DoseRepository
    {
        //private PassVaxDbContext db = new PassVaxDbContext();


        public void AddDose(Dose p)
        {
            using (var db = new PassVaxDbContext())
            {
                db.Doses.Add(p);
                db.SaveChanges();
            }
        }

        public bool DeleteDose(Guid id)
        {
            return true;
        }
        public List<Dose> GetDoses()
        {
            using (var db = new PassVaxDbContext())
            {
                var Doses = db.Doses.ToList();
                return Doses;
            }
        }

        public Dose GetDoseById(Guid id)
        {
            using (var db = new PassVaxDbContext())
            {
                var Dose = db.Doses.FirstOrDefault(x => x.Id == id);
                return Dose;

            }
        }
        public void EditDose(Dose pg)
        {
            using (var db = new PassVaxDbContext())
            {
                db.Entry(pg).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}

