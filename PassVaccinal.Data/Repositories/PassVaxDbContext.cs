﻿namespace PassVaccinal.Data
{
    using PassVaccinal.Data.Models;
    using System.Data.Entity;

    public class PassVaxDbContext : DbContext
    {
        public DbSet<Patient> Patients { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Dose> Doses { get; set; }
        public DbSet<Disease> Diseases { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("RAJOSIARISAONAMI2122");
        }
    }
}
