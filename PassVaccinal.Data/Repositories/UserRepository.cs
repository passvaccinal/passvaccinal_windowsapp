﻿namespace PassVaccinal.Data.Repositories
{
    using PassVaccinal.Data.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    public class UserRepository
    {
        public void AddUser(User u)
        {
            using (var db = new PassVaxDbContext())
            {
                var user = db.Users.FirstOrDefault(x => x.UserName == u.UserName);
                if (user != null)
                {
                    throw new InvalidOperationException("user name already exist");
                }
                db.Users.Add(u);
                db.SaveChanges();
            }
        }

        public bool DisableUser(Guid id)
        {
            using (var db = new PassVaxDbContext())
            {
                User u = db.Users.FirstOrDefault(x => x.Id == id);
                if (u != null)
                {
                    u.IsDisabled = true;
                    this.EditUser(u);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<User> GetUsers(bool fullList = false)
        {
            using (var db = new PassVaxDbContext())
            {
                var users = new List<User>();
                if (fullList)
                {
                    users = db.Users.ToList();
                }
                else
                {
                    users = db.Users.Where(x => !x.IsDisabled).ToList();
                }

                return users;
            }
        }

        public User GetUserById(Guid id)
        {
            using (var db = new PassVaxDbContext())
            {
                var user = db.Users.FirstOrDefault(x => x.Id == id);
                return user;
            }

        }
        public User FindUser(string username, string pwd)
        {
            using (var db = new PassVaxDbContext())
            {
                var user = db.Users.FirstOrDefault(x => x.UserName == username && x.Password == pwd && !x.IsDisabled);
                return user;
            }

        }

        public void EditUser(User u)
        {
            using (var db = new PassVaxDbContext())
            {
                db.Entry(u).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}

