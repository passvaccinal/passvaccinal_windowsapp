﻿namespace PassVaccinal.Data.Repositories
{
    using PassVaccinal.Data.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    public class PatientRepository
    {
        private PassVaxDbContext db = new PassVaxDbContext();

        public void AddPatient(Patient t)
        {
            using (var db = new PassVaxDbContext())
            {
                var Patient = db.Patients.FirstOrDefault(x => x.Cin == t.Cin);
                if (Patient != null)
                {
                    throw new InvalidOperationException("Patient name already exist");
                }
                db.Patients.Add(t);
                db.SaveChanges();
            }
        }

        public bool DeletePatient(Guid id)
        {
            using (var db = new PassVaxDbContext())
            {
                var Patient = db.Patients.FirstOrDefault(x => x.Id == id);
                if (Patient != null)
                {
                    db.Patients.Remove(Patient);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<Patient> GetPatients()
        {
            using (var db = new PassVaxDbContext())
            {
                var Patients = db.Patients.ToList();
                return Patients;
            }
        }

        public Patient GetPatientById(Guid id)
        {
            using (var db = new PassVaxDbContext())
            {
                var Patient = db.Patients.FirstOrDefault(x => x.Id == id);
                return Patient;

            }
        }

        public void EditPatient(Patient t)
        {
            using (var db = new PassVaxDbContext())
            {
                db.Entry(t).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
