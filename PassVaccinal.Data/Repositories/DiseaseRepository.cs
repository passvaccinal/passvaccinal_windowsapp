﻿namespace PassVaccinal.Data.Repositories
{
    using PassVaccinal.Data.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    public class DiseaseRepository
    {
        //private PassVaxDbContext db = new PassVaxDbContext();

        public void AddDisease(Disease f)
        {
            using (var db = new PassVaxDbContext())
            {
                db.Diseases.Add(f);
                db.SaveChanges();
            }
        }

        public bool DeleteDisease(Guid id)
        {
            return true;
        }

        public List<Disease> GetDiseases()
        {
            using (var db = new PassVaxDbContext())
            {
                var disease = db.Diseases.ToList();
                return disease;
            }
        }

        public Disease GetDiseaseById(Guid id)
        {
            using (var db = new PassVaxDbContext())
            {
                Disease Disease = db.Diseases.FirstOrDefault(x => x.Id == id);
                if (Disease != null)
                {
                    return Disease;
                }
                else
                {
                    throw new InvalidOperationException("Disease doesn't exist yet");
                }
            }
        }
        public List<Disease> GetDiseasesByUserId(Guid idPatient)
        {
            using (var db = new PassVaxDbContext())
            {
                List<Disease> Diseases = db.Diseases.Where(x => x.PatientId == idPatient).ToList();
                return Diseases;
            }
        }

        public void EditDisease(Disease f)
        {
            using (var db = new PassVaxDbContext())
            {
                db.Entry(f).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
